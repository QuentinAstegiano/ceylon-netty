native("jvm")
module org.astegiano.ceylon.netty "1.0.0" {
    import ceylon.interop.java "1.3.1";
    import ceylon.regex "1.3.1";
    shared import maven:"io.netty:netty-all" "4.1.6.Final";

}
