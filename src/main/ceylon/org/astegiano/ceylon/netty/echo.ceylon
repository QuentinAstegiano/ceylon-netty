import ceylon.interop.java {
    javaClass
}

import io.netty.bootstrap {
    ServerBootstrap
}
import io.netty.channel {
    ChannelInboundHandlerAdapter,
    ChannelHandlerContext,
    EventLoopGroup,
    ChannelInitializer,
    ChannelOption,
    ChannelFuture
}
import io.netty.channel.nio {
    NioEventLoopGroup
}
import io.netty.channel.socket {
    SocketChannel
}
import io.netty.channel.socket.nio {
    NioServerSocketChannel
}

import java.lang {
    JavaInteger=Integer,
    JavaBoolean=Boolean
}

shared class EchoServerHandler() extends ChannelInboundHandlerAdapter() {

    shared actual void channelRead(ChannelHandlerContext? ctx, Object? msg) {
        if (exists ctx, exists msg) {
            ctx.write(msg);
            ctx.flush();
        }
    }

    shared actual void exceptionCaught(ChannelHandlerContext? ctx, Throwable? cause) {
        if (exists cause, exists ctx) {
            cause.printStackTrace();
            ctx.close();
        }
    }
}

shared class EchoServer(Integer port = 9999) {

    class ChildHandler() extends ChannelInitializer<SocketChannel>() {
        shared actual void initChannel(SocketChannel? ch) => ch ?. pipeline() ?. addLast(EchoServerHandler());
    }

    shared void startServer() {
        EventLoopGroup bossGroup = NioEventLoopGroup();
        EventLoopGroup workerGroup = NioEventLoopGroup();

        try {
            ServerBootstrap b = ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(javaClass<NioServerSocketChannel>())
                .childHandler(ChildHandler())
                .option(ChannelOption.soBacklog, JavaInteger(128))
                .childOption(ChannelOption.soKeepalive, JavaBoolean(true));

            print("Echo server started on port ``port``");
            ChannelFuture f = b.bind(port).sync();

            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}