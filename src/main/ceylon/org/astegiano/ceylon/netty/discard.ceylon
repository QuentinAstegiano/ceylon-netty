import ceylon.interop.java {
    javaClass
}
import java.lang {
    JavaInteger = Integer,
    JavaBoolean = Boolean
}
import io.netty.bootstrap {
    ServerBootstrap
}
import io.netty.channel {
    ChannelInboundHandlerAdapter,
    ChannelHandlerContext,
    EventLoopGroup,
    ChannelInitializer,
    ChannelOption,
    ChannelFuture
}
import io.netty.channel.nio {
    NioEventLoopGroup
}
import io.netty.channel.socket.nio {
    NioServerSocketChannel
}
import io.netty.util {
    ReferenceCounted
}
import io.netty.channel.socket {
    SocketChannel
}


shared class DiscardServerHandler() extends ChannelInboundHandlerAdapter() {

    shared actual void channelRead(ChannelHandlerContext? ctx, Object? msg) {
        if (is ReferenceCounted msg) {
            msg.release();
        }
    }

    shared actual void exceptionCaught(ChannelHandlerContext? ctx, Throwable? cause) {
        if (exists cause, exists ctx) {
            cause.printStackTrace();
            ctx.close();
        }
    }
}

shared class DiscardServer(Integer port = 9999) {

    class ChildHandler() extends ChannelInitializer<SocketChannel>() {
        shared actual void initChannel(SocketChannel? ch) => ch?.pipeline()?.addLast(EchoServerHandler());
    }

    shared void startServer() {
        EventLoopGroup bossGroup = NioEventLoopGroup();
        EventLoopGroup workerGroup = NioEventLoopGroup();

        try {
            ServerBootstrap b = ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(javaClass<NioServerSocketChannel>())
                .childHandler(ChildHandler())
                .option(ChannelOption.soBacklog, JavaInteger(128))
                .childOption(ChannelOption.soKeepalive, JavaBoolean(true));

            print("Discard server started on port ``port``");
            ChannelFuture f = b.bind(port).sync();

            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}